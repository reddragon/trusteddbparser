#if !defined(_SCHEMA_H_)
#define _SCHEMA_H_

// TODO Change #defines to SELECT_OP, FROM_OP, ... 
// or something similar before integrating with the
// main parser.
/*
#define SELECT	"select"
#define FROM	"from"
#define WHERE	"where"
#define AND	"and"
*/

// TODO Uncomment this when integrating with the main
// parser.
#define HOST_MYSQL_REQ 0x0001
#define HOST_OS_REQ 0x0002
#define HOST_SQLITE_REQ 0x0004

#define NAME_LEN 30
#define CONST_LEN 20
#define OPERATOR_LEN 5

struct column
{
	char table_alias;
	char name[NAME_LEN];
	char is_private;
	char aggr_index;
	struct column* next;
};

struct table
{
	char alias;
	char name[NAME_LEN];
	struct table* next;
};

struct where_clause
{
	union 
	{
		struct column col;
		char constant[CONST_LEN];
	} left_element;
	char operator[OPERATOR_LEN];
	union 
	{
		struct column col;
		char constant[2 * NAME_LEN];
	} right_element;
	char is_private_involved;
	char is_left_column[2];
	char is_right_column[2];
	struct where_clause* next;
};

typedef struct schema_column
{
	char name[NAME_LEN];
	char type[NAME_LEN];
	char encrypted;
} attr;

struct schema_table
{
	char name[NAME_LEN];
	char alias;
	int no_cols;
	struct schema_column column_list[];
};

struct select_clause
{
	char *clause;
	char *alias;
	char is_aggr;
	char aggrind;
	char type[NAME_LEN];
};

struct query_sequence
{
	char *query;
	char incharge;
	int type;
	struct query_sequence *next;
};

#define NOOF_TABLES 8

#endif
