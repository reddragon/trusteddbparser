CXX = gcc
LEX = flex
YACC = bison
CFLAGS = -O2 -Wall -g
INC=/usr/local/mysql/include/

all: parser

tester: lex.yy.c parser.tab.h parser.tab.c tester.c datadef.c query.c query.h test_queries.h
	$(CXX) $(CFLAGS) lex.yy.c parser.tab.h parser.tab.c datadef.c query.c tester.c -o parser

parser: lex.yy.c parser.tab.h parser.tab.c datadef.c query.c query.h test_queries.h base64.o sha.o demo.o 
	$(CXX) $(CFLAGS) lex.yy.c parser.tab.h parser.tab.c datadef.c query.c base64.o sha.o demo.o -o parser -lsqlite3 -lpthread -ldl -lz -L/usr/local/mysql/lib/ -lmysqlclient

parser.tab.c parser.tab.h: parser.y
	$(YACC) -d --report=all parser.y

lex.yy.c: lex.l parser.tab.h
	$(LEX) lex.l

demo.o: demo.c
	$(CXX) -I$(INC) -L/usr/lib/mysql/ -c demo.c -o $@

base64.o: base64.c
	$(CXX) -I$(INC) -c base64.c -o $@

sha.o: sha.c
	$(CXX) -I$(INC) -c sha.c -o $@

clean:
	rm parser parser.tab.c parser.tab.h lex.yy.c parser.output
	rm -rf parser.dSYM/
	rm *.o
