%{
#include "parser.tab.h"

#define RETURN_STYPE(STYPE) { \
  yylval.id = strdup(yytext); \
  return STYPE; \
}

#define RETURN_CTYPE return yytext[0];
#define RETURN_STR return yytext

%}

%option noyywrap
%option yylineno

%%

(select)|(SELECT) { RETURN_STYPE(SELECT); }
(from)|(FROM) { RETURN_STYPE(FROM); }
(where)|(WHERE) { RETURN_STYPE(WHERE); }
(and)|(AND) { RETURN_STYPE(AND); }
(as)|(AS) { RETURN_STYPE(AS); }
(or)|(OR) { RETURN_STYPE(OR); }
(group\ by)|(GROUP\ BY) { RETURN_STYPE(GROUP_BY); }
(order\ by)|(ORDER\ BY) { RETURN_STYPE(ORDER_BY); }
(desc)|(DESC) { RETURN_STYPE(DESC); }
(sum)|(SUM) { RETURN_STYPE(SUM); }
(avg)|(AVG) { RETURN_STYPE(AVG); }
(\'|\")([\[\]\%\#._\ a-zA-Z0-9]|[-])*(\'|\") { RETURN_STYPE(STR); } 
(in)|(IN) { RETURN_STYPE(IN); }
(count)|(COUNT) { RETURN_STYPE(COUNT); }
(distinct)|(DISTINCT) { RETURN_STYPE(DISTINCT); }
(between)|(BETWEEN) { RETURN_STYPE(BETWEEN); }
(like)|(LIKE) { RETURN_STYPE(LIKE); }
(having)|(HAVING) { RETURN_STYPE(HAVING); }
(EXISTS)|(exists) { RETURN_STYPE(EXISTS); }
(NOT)|(not) { RETURN_STYPE(NOT); }
(left)|(LEFT) { RETURN_STYPE(LEFT); }
(outer)|(OUTER) { RETURN_STYPE(OUTER); }
(join)|(JOIN) { RETURN_STYPE(JOIN); }
(on)|(ON) { RETURN_STYPE(ON); }
(substr)|(SUBSTR) { RETURN_STYPE(SUBSTR); }
(>=)|(<=)|(<>)|(=)|(<)|(>) { RETURN_STYPE(CMP_OP); }
[._a-zA-Z][._a-zA-Z0-9]* { RETURN_STYPE(IDENTIFIER); }
[0-9]*|([0-9]*\.[0-9]*)  { RETURN_STYPE(NUMBER); }
, { RETURN_STYPE(COMMA); }
; { RETURN_STYPE(QEND); }
[+*\-\(\)] { RETURN_CTYPE; }
[ \n] ;
. ;
%%

/*
int
main() {
  yylex();
  return 0;
} */
