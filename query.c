#include "query.h"
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <assert.h>

extern BOOL hasPrivateFields;
extern const int attr_list_sz;
extern attr attr_list[];
BOOL useShortName, inWhereClause;
AliasColumn *aHead;

/*
 *	This function, given the Column, gets the appropriate name for it. If
 *	the column is public, it would just return the actual name. If it is 
 *	private, it would prepend 'tdb_decrypt' in fron of the name. 
 */
char *
getColumnText(struct Column *c) {
	// printf("testing for %s %d\n", c->columnText, getAccessType(c->columnText));
	char *cstr = c->columnText;
	if (useShortName) {
		cstr = getShortName(cstr);
	}
	if (getAccessType(c->columnText) == PUBLIC) {
		return cstr;
	} else {
		char *str = (char *) malloc(sizeof(char) * (strlen(c->columnText) + 20));
		str[0] = 0;
		strcat(str, "tdb_decrypt(");
		strcat(str, cstr);
		strcat(str, ")");
		// printf("%s -> %s\n", c->columnText, str);
		return str;
	}
}

/*
 * As the name suggests, given the operator string, this
 * function returns the enum PredicateOperator according
 * to the operator string. This helps in comparisons, without
 * doing strinc comparisons.
 */
enum 
PredicateOperator strToOperator(char* str) {
	if (!strcmp(str, ">=")) {
		return GEQ;
	} else if (!strcmp(str, "<=")) {
		return LEQ;
	} else if (!strcmp(str, "=")) {
		return EQ;
	} else if (!strcmp(str, ">")) {
		return GT;
	} else if (!strcmp(str, "<")) {
		return LT;
	} else if (!strcmp(str, "<>")) {
		return NEQ;
	} else return BETWEEN_OP;
}

/* This is not a very generic way to get the 
 * access type. We compare the entire name of the
 * column, AS IT IS, with a set of column names
 * that we know are private in the benchmark. This
 * column list is populated before-hand.
 */
enum
AccessType getAccessType(char *str) {
	struct PrivateColumn *p = pColumns;
	if (p == NULL) {
		printf("p is NULL\n");
	}
	BOOL hasProperName = FALSE;
	UINT i = 0;
	for (i = 0; i < strlen(str); i++) {
		if (str[i] == '.') {
			hasProperName = TRUE;
			break;
		}
	}
	char *properName;
	if (hasProperName) {
		properName = str + i + 1;
	}
	while (p != NULL) {
		//printf("str: %s, pct: %s hpn: %d %s\n", str, p->columnText, hasProperName, properName);
		if (!strcasecmp(p->columnText, str) ||
				(hasProperName && !strcasecmp(p->columnText, properName))) {
			return PRIVATE;
		}
		p = p->next;
	}
	return PUBLIC;
}

/* 
 * A filler method, which is called initially
 * to get the data-type of a column. I intended
 * to look up the pre-defined column list here, 
 * and return the data-type, but I ended up doing
 * it in the dataTypeToStr() method, since the
 * we can not always figure out the data-type of
 * a column, just by the name.
 */
enum
DataType getDataType(char *str) {
	return INTEGER;
}

/*
 * This is the method which actually finds out the
 * data-type of a column. If the column is an aggregation,
 * it assumes by default that it is an integer. Change
 * this behavior if required.
 *
 * Otherwise, it checks it first resolves aliases, and then
 * looks up in the column list to find the data type for
 * the actual column.
 */
char *dataTypeToStr(struct Column *c) {
	if (c->columnType == AGGREGATION) {
		struct Aggregation *a = c->column.aggregation;
		processAggregation(a);
		int i;
		for (i = 0; i < *(a->allColumnsLen); i++) {
			return dataTypeToStr(a->allColumns + i);	
		}
		return "integer";
	}
	char *str = c->columnText;
	BOOL hasProperName = FALSE;
	UINT i = 0;
	for (i = 0; i < strlen(str); i++) {
		if (str[i] == '.') {
			hasProperName = TRUE;
			break;
		}
	}
	char *properName;
	if (hasProperName) {
		properName = str + i + 1;
	} else {
		properName = c->columnText;
	}
	for (i = 0; i < attr_list_sz; i++) {
		// fprintf(stderr, "%s %s\n", attr_list[i].name, c->columnText);
		if (!strcasecmp(attr_list[i].name, properName)) {
			return attr_list[i].type;
		}
	}

	return "<NO-DATATYPE>";
}

/*
 * This function checks if a given column is Private
 * or not. If this is an aggregation column, all its
 * column should be public, in order for the
 * aggregation to be public itself.
 */
BOOL
isPrivateColumn(struct Column *c) {
	if (c != NULL && c->accessType == PRIVATE) {
		return TRUE;
	}
	if (c != NULL && c->columnType == AGGREGATION) {
		struct Aggregation *a = c->column.aggregation;
		if (a->queryStr == NULL) {
			processAggregation(a);
			if (a->hasPrivateColumns) {
				return TRUE;
			}
		}
	}
	return FALSE;
}

/*
 * Checks if the where clause is private, by checking
 * if either of the LHS or RHS is private.
 */
BOOL
isPrivateWhereClause(struct WhereClause *w) {
	if (w->leftColumn != NULL && w->leftColumn->columnType == AGGREGATION) {
		if (w->leftColumn->column.aggregation->aggregationType == AGGREGATION_SUBSTR) {
			return TRUE;
		}
	}
	if (isPrivateColumn(w->leftColumn) || isPrivateColumn(w->rightColumn) || (w->eQuery != NULL && w->eQuery->hasPrivate)) {
		return TRUE;
	}
	return FALSE;
}

/*
 * This function converts a given where clause struct
 * to a string, which can be inserted in the query.
 */
#define MAXWCLEN 5000
char *whereClauseToStr(struct WhereClause *w, BOOL allowPrivates) {
	char *whereStr = (char *) malloc(sizeof(char) * MAXWCLEN);
	inWhereClause = TRUE;
	
	// Check if the left column is an aggregation
	if (w->leftColumn != NULL && w->leftColumn->columnType == AGGREGATION) {
		struct Aggregation *a = w->leftColumn->column.aggregation;
		processAggregation(a);
		// If the aggregation has private columns, put
		// the string in encQueryStr, which is the 
		// encrypted query string
		if (a->hasPrivateColumns) {
			strcpy(whereStr, a->encQueryStr);
		} else {
			strcpy(whereStr, a->queryStr);
		}
	}

	// We will handle the BETWEEN operator separately
	if (w->predicateOperator != BETWEEN_OP) {
		if (allowPrivates && isPrivateColumn(w->leftColumn)) {
			strcat(whereStr, "tdb_decrypt(");
			char *str = w->lhsField;

			// useShortName is turned on once we have defined aliases
			if (w->leftColumn != NULL && useShortName) {
				// printf("ColumnType: %d ShortName: %s\n", w->leftColumn->columnType, getShortName(w->leftColumn->columnText));
				str = getAptColumnName(w->leftColumn);
			}
			if (str != NULL) {
				strcat(whereStr, str);
			}
			strcat(whereStr, ")");
		} else {
			char *str = w->lhsField;
			if (w->leftColumn != NULL && w->leftColumn->columnType == COLUMN) {
				str = getAptColumnName(w->leftColumn);
			}
			if (w->lhsField != NULL) {
				strcat(whereStr, w->lhsField);
			}
		}
		if (w->predicateOperator != NEX && w->predicateOperator != EX) {
			strcat(whereStr, " ");
		}
		switch (w->predicateOperator) {
			case GEQ:
				strcat(whereStr, ">=");
				break;
			case LEQ:
				strcat(whereStr, "<=");
				break;
			case EQ:
				strcat(whereStr, "=");
				break;
			case GT:
				strcat(whereStr, ">");
				break;
			case LT:
				strcat(whereStr, "<");
				break;
			case NEQ:
				strcat(whereStr, "<>");
				break;
			case LIKE_OP:
				strcat(whereStr, "LIKE");
				break;
			case NOT_LIKE_OP:
				strcat(whereStr, "NOT LIKE");
				break;
			case EX:
				strcat(whereStr, "EXISTS");
				break;
			case NEX:
				strcat(whereStr, "NOT EXISTS");
				break;
			case IN_OP:
				strcat(whereStr, "IN ");
				break;
			default:
				break;
		}
		if (w->predicateOperator == EX ||
				w->predicateOperator == NEX || 
				(w->eQuery != NULL)) {
			assert(w->eQuery != NULL);
			strcat(whereStr, " ");
			char *nstr = w->eQuery->qstr;
			if (w->eQuery->hasPrivate) {
				//nstr = w->eQuery->q->encQueryStr;
				strcat(whereStr, " tdb_decrypt");
			}
			
			strcat(whereStr, "(");
			replaceChars(nstr, '\n', ' ');
			replaceChars(nstr, '\t', ' ');
			removeExtraSpaces(nstr);
			strcat(whereStr, w->eQuery->qstr);
			strcat(whereStr, ")");
		} else if (allowPrivates && isPrivateColumn(w->rightColumn)) {
			strcat(whereStr, " ");
			strcat(whereStr, "tdb_decrypt(");
			strcat(whereStr, getAptColumnName(w->rightColumn));
			strcat(whereStr, ")");
		} else if (w->rhsField != NULL) {
			strcat(whereStr, " ");
			if (w->predicateOperator == IN_OP) {
				strcat(whereStr, "(");
			}
			char *rstr = w->rhsField;
			if (w->rightColumn != NULL) {
				rstr = getAptColumnName(w->rightColumn);
			}
			strcat(whereStr, rstr);
			if (w->predicateOperator == IN_OP) {
				strcat(whereStr, ")");
			}
		}   
	} else {
		// ASSUMPTION
		// Of the format: column BETWEEN value1 AND VALUE2
		if (allowPrivates && isPrivateColumn(w->leftColumn)) {
			char *lstr = getAptColumnName(w->leftColumn);
			strcat(whereStr, "tdb_decrypt(");
			strcat(whereStr, lstr);
			strcat(whereStr, ")");
		} else if (w->lhsField != NULL) {
			char *lstr = w->lhsField;
			if (w->leftColumn != NULL) {
				lstr = getAptColumnName(w->leftColumn);
			}
			strcat(whereStr, lstr);
		}
		strcat(whereStr, " BETWEEN ");
		if (w->rhsField != NULL && whereStr != NULL) {
			strcat(whereStr, w->rhsField);
		}
	} 
	inWhereClause = FALSE;
	return whereStr;
}


/*
 * This function processes aggregation expression structs
 * and fills in str - the expression converted to a string,
 * and eqstr - the expression converted to a string, along
 * with the encryption / decryption function calls.
 */
void
processAggrExp(struct Expression *e, char *qstr, char *eqstr, struct Column *c, UINT *len) {
	char *str;
	if (e == NULL) {
		*len = 0;
		return;
	}
	switch (e->expressionType) {
		case NESTED_EXPRESSION:
			strcat(qstr, "(");
			strcat(eqstr, "(");
			processAggrExp(e->exp.ne->lhs, qstr, eqstr, c, len);
			switch (e->exp.ne->binaryOp) {
				case '+':
					strcat(qstr, " + ");
					strcat(eqstr, " + ");
					break;
				case '-':
					strcat(qstr, " - ");
					strcat(eqstr, " - ");
					break;
				case '*':
					strcat(qstr, " * ");
					strcat(eqstr, " * ");
					break;
				default:
					break;
			}
			processAggrExp(e->exp.ne->rhs, qstr, eqstr, c, len);
			strcat(qstr, ")");
			strcat(eqstr, ")");
			break;
		case VALUE_EXPRESSION:
			strcat(qstr, e->exp.value);
			strcat(eqstr, e->exp.value);
			break;
		case COLUMN_EXPRESSION:
			str = getAptColumnName(e->exp.column);
			strcat(qstr, str);
			if (e->exp.column->accessType == PRIVATE) {
				strcat(eqstr, "tdb_decrypt(");

				strcat(eqstr, str);
				strcat(eqstr, ")");
			} else {
				strcat(eqstr, str);
			}
			*(c + *len) = *(e->exp.column);
			(*len)++;
			break;
		default:
			break;
	}
}

/*
 * This is a wrapper around processAggrExp()
 */
void
processAggregation(struct Aggregation *a) {
	char *qstr = (char *) malloc(sizeof(char) * MAXQLEN);
	char *eqstr = (char *) malloc(sizeof(char) * MAXQLEN);
	qstr[0] = 0;
	eqstr[0] = 0;
	a->allColumns = (struct Column *) malloc(sizeof(struct Column) * MAXCOLS);
	a->allColumnsLen = (UINT *) malloc(sizeof(UINT));
	if (a->aggregationType == AGGREGATION_SUM) {
		strcat(qstr, "SUM(");
		strcat(eqstr, "tdb_encrypt(SUM(");
	} else if (a->aggregationType == AGGREGATION_AVG) {
		strcat(qstr, "AVG(");
		strcat(eqstr, "tdb_encrypt(AVG(");
	} else if (a->aggregationType == AGGREGATION_COUNT) {
		strcat(qstr, "COUNT(");
		strcat(eqstr, "tdb_encrypt(COUNT(");
	} else if (a->aggregationType == AGGREGATION_COUNT_D) {
		strcat(qstr, "COUNT(DISTINCT ");
		strcat(eqstr, "tdb_encrypt(COUNT(DISTINCT");
	} else if (a->aggregationType == AGGREGATION_SUBSTR) {
		strcat(qstr, "SUBSTR(");
		strcat(eqstr, "SUBSTR(");
		a->hasPrivateColumns = TRUE;
	}


	*a->allColumnsLen = 0;
	processAggrExp(a->expression, qstr, eqstr, a->allColumns, a->allColumnsLen); 
	if (a->aggregationType == AGGREGATION_COUNT &&
			a->expression == NULL) {
		strcat(qstr, "*");
	}
	if (a->aggregationType == AGGREGATION_SUBSTR) {
		strcat(qstr, ", 1, 2");
		strcat(eqstr, ", 1, 2");
	}
	strcat(qstr, ")");
	strcat(eqstr, "), 1)");
	if (a->alias != NULL) {
		strcat(qstr, " AS ");
		strcat(eqstr, " AS ");
		strcat(qstr, a->alias);
		strcat(eqstr, a->alias);
	}
	UINT i;
	assert(*a->allColumnsLen <= MAXCOLS);
	for (i = 0; i < *a->allColumnsLen; i++) {
		if (a->allColumns[i].accessType == PRIVATE) {
			a->hasPrivateColumns = TRUE;
			break;
		}
	}
	a->queryStr = qstr;
	a->encQueryStr = eqstr;
}

/*
 * This function creates the list of all the columns present
 * in the select part of the query. Since some columns might
 * be present in aggregations, we need to use this function
 */
void
allColumnsInSelect(struct Column *c, struct Column *sc, UINT *ind) {
	struct Column *t;
	t = sc;
	// First copy all the columns in the SELECT part
	while (t != NULL) {
		// printf("ind: %d\n", *ind);
		if (t->columnType == COLUMN || t->columnType == ALLCOLUMNS) {
			c[*ind] = *t;
			(*ind)++;
		} else if (t->columnType == AGGREGATION) {
			struct Aggregation *a = t->column.aggregation;
			processAggregation(a);
			UINT i;
			for (i = 0; i < *(a->allColumnsLen); i++) {
				struct Column *aggrC = a->allColumns + i;
				BOOL exists = FALSE;
				UINT j;
				//printf("i: %d, s: %s, ind: %d\n", i, aggrC->columnText, *ind);
				for (j = 0; j < *ind; j++) {
					if (c[j].columnText == NULL) {
						printf("Column is null, i: %d\n", i);
					} else if (aggrC->columnText == NULL) {
						printf("aggr is null\n");
					}
					if (!strcmp(c[j].columnText, aggrC->columnText)) {
						exists = TRUE;
						break;
					}
				}
				if (!exists) {
					c[*ind] = *aggrC;
					//printf("str: %s\n", c[*ind].columnText);
					(*ind)++;
				}
			}
		} 
		t = t->next;
	}
}

/*
 * This is similar to allColumnsInSelect, and it finds the list of
 * all columns in the Where Clauses 
 */
void
allColumnsInWhere(struct Column *c, struct WhereClause *w, UINT *ind) {
	UINT i;
	while (w != NULL) {
		if (w->leftColumn != NULL && isPrivateColumn(w->leftColumn)) {
			BOOL exists = FALSE;
			for (i = 0; i < *ind; i++) {
				if (w->leftColumn->columnText != NULL && !strcmp(c[i].columnText, w->leftColumn->columnText)) {
					exists = TRUE;
					break;
				}
			}
			
			if (w->leftColumn == AGGREGATION) {
				struct Aggregation *a = w->leftColumn->column.aggregation;
				if (a->aggregationType == AGGREGATION_SUBSTR) {
					exists = TRUE;
				}
			}

			if (!exists) {
				c[*ind] = *w->leftColumn;
				(*ind)++;
			}
		}

		if (w->rightColumn != NULL && isPrivateColumn(w->rightColumn)) {
			BOOL exists = FALSE;
			for (i = 0; i < *ind; i++) {
				if (!strcmp(c[i].columnText, w->rightColumn->columnText)) {
					exists = TRUE;
					break;
				}
			}	
			if (!exists) {
				c[*ind] = *w->rightColumn;
				(*ind)++;
			}
		}
		w = w->next;
	}
}

void
allColumnsInWhere_All(struct Column *c, struct WhereClause *w, UINT *ind) {
	UINT i;
	while (w != NULL) {
		if (w->leftColumn != NULL && w->leftColumn->columnText != NULL) {
			BOOL exists = FALSE;
			for (i = 0; i < *ind; i++) {
				if (!strcmp(c[i].columnText, w->leftColumn->columnText)) {
					exists = TRUE;
					break;
				}
			}
			if (!exists) {
				c[*ind] = *w->leftColumn;
				(*ind)++;
			}
		}
		if (w->rightColumn != NULL) {
			BOOL exists = FALSE;
			for (i = 0; i < *ind; i++) {
				if (!strcmp(c[i].columnText, w->rightColumn->columnText)) {
					exists = TRUE;
					break;
				}
			}	
			if (!exists) {
				c[*ind] = *w->rightColumn;
				(*ind)++;
			}
		}
		w = w->next;
	}
}


/*
 * This is a helper function, which checks if the where clause is 'allowed',
 * as per the 'access' variable.
 */
BOOL
isAllowedWhereClause(struct WhereClause *whereClause, enum AccessType access) {
	if ((access == PUBLIC && !isPrivateWhereClause(whereClause)) ||
			(access == PRIVATE && isPrivateWhereClause(whereClause))) {
		return TRUE;
	}
	return FALSE;
}

/*
 * Gets all the where clauses as a concatenated string, as per
 * the access.
 */
void
allWhereClauses(struct WhereClause *whereClause, char *wcstr, int *wclen, enum AccessType access) {	
	while (whereClause != NULL) {
		if (isAllowedWhereClause(whereClause, access)) {
			strcat(wcstr, whereClauseToStr(whereClause, (access == PRIVATE)));
			whereClause = whereClause->next;
			(*wclen)++;
		}

		while (whereClause != NULL && !isAllowedWhereClause(whereClause, access)) {
			whereClause = whereClause->next;
		}

		if (whereClause != NULL && *wclen > 0) {
			strcat(wcstr, " AND\n\t");  
		}
		strcat(wcstr, " ");
	}
}

/*
 * Gets the list of all public where clauses as a string.
 */
void
allPublicWhereClauses(struct WhereClause *whereClause, char *wcstr, int *wclen) {
	allWhereClauses(whereClause, wcstr, wclen, PUBLIC);
}

/*
 * Gets the list of all private where clauses as a string.
 */ 
void
allPrivateWhereClauses(struct WhereClause *whereClause, char *wcstr, int *wclen) {
	allWhereClauses(whereClause, wcstr, wclen, PRIVATE);
}

/*
 * This function creates a string out of the group by clause
 * according to the access type.
 */
void
processGroupBy(struct GroupByClause *g, char *gbstr, enum AccessType accessType) {
	if (g == NULL) {
		return;
	}
	gbstr[0] = 0;
	struct Column *t;
	t = g->columns;
	while (t != NULL) {
		// If we want only PUBLIC group by
		if (t->accessType == PRIVATE && accessType == PUBLIC) {
			// We will have an empty gbstr
			gbstr[0] = 0;
			return;
		}
		strcat(gbstr, getColumnText(t));
		t = t->next;
		if (t != NULL) {
			strcat(gbstr, ", ");
		}
	}

	if (g->conditions != NULL) {
		strcat(gbstr, " HAVING ");
		char wcstr[MAXQLEN];
		int wclen = 0;
		wcstr[0] = 0;
		allPublicWhereClauses(g->conditions, wcstr, &wclen);
		strcat(gbstr, wcstr);
	}
}

/*
 * This method creates a string out of the order by clause
 */
void
processOrderBy(struct OrderByClause *o, char *obstr) {
	if (o == NULL) {
		return;
	}

	struct Column *t;

	while (o != NULL) {
		t = o->column;
		char *str = t->columnText;
		if (useShortName) {
			str = getShortName(str);
		}

		AliasColumn *a = aHead;
		while (a != NULL) {
			if (!strcmp(a->c->alias, t->columnText)) {
				if (a->c->accessType == PRIVATE) {
					t->accessType = PRIVATE;
					break;
				}
			}
			a = a->next;
		}

		//printf("str: %s ac: %d\n", str, t->accessType);
		if (t->accessType == PRIVATE) {
			strcat(obstr, "tdb_decrypt(");
			strcat(obstr, str);
			strcat(obstr, ")");
		} else {
			strcat(obstr, str);
		}
		if (o->order) {
			strcat(obstr, " ");
			strcat(obstr, o->order);
		}

		o = o->next;
		if (o != NULL) {
			strcat(obstr, ", \n\t");
		}
	}
}

/*
 * This function checks if a given set of columns has a private
 * column amongst them 
 */
BOOL
hasPrivateColumns(struct Column *c, struct Column *allColumns) {
	struct Column *t;
	t = c;
	BOOL hasPrivate = FALSE;
	while (t != NULL) {
		if (t->columnType == COLUMN) {
			// Check if this is a column that might be an aggregation
			struct Column *p = allColumns;
			while (p != NULL) {
				if (p->columnType == AGGREGATION &&
						!strcasecmp(p->column.aggregation->alias, t->column.columnText) && p->column.aggregation->hasPrivateColumns == TRUE) {
					t->accessType = PRIVATE; 
					hasPrivate = TRUE;
					break;
				}
				p = p->next;
			}

			AliasColumn *a = aHead;
			while (a != NULL) {
				// Check if this column is actually an alias of a private column
				if (!strcmp(a->c->alias, t->columnText) && a->c->accessType == PRIVATE) {
					t->accessType = PRIVATE;
					break;
				}
				a = a->next;
			}

			if (t->accessType == PRIVATE) {
				hasPrivate = TRUE;
			}
		} else if (t->columnType == AGGREGATION) {
			if (t->column.aggregation->hasPrivateColumns) {
				hasPrivate = TRUE;
			}
		}
		t = t->next;
	}
	return hasPrivate;
}

/*
 * This function checks if the order by has any private columns
 * Is a wrapper around hasPrivateColumns for order by
 */
BOOL
hasPrivateOrderByColumns(struct OrderByClause *o, struct Column *allCols) {
	BOOL hasPrivate = FALSE;
	while (o != NULL) {
		hasPrivate |= hasPrivateColumns(o->column, allCols);
		if (hasPrivate) {
			break;
		}
		o = o->next;
	}
	return hasPrivate;
}

/*
 * Creates a query_sequence node, which is essentially a query string
 * that needs to be executed
 */
struct query_sequence *
create_qs_node(char *str, char incharge, int type) {
	struct query_sequence *qs = (struct query_sequence *) 
		malloc(sizeof(struct query_sequence));
	qs->query = (char *) malloc(sizeof(char) * (strlen(str) + 1));
	strcpy(qs->query, str);
	qs->incharge = incharge;
	qs->type = type;
	qs->next = NULL;
	return qs;
}

/*
 * This is a special function which deals with converting
 * a nested query struct into a string. Similar to processQuery,
 * but it assumes that the nested query has all public columns.
 */
void
nestedQueryToStr(NestedQuery *nq) {
	char qstr[MAXQLEN], nqstr[MAXQLEN];
	struct Query *q = nq->q;
	struct Column *c, *t;
	struct WhereClause *w;
  struct NestedWhereClause *n;
  
  UINT ind = 0, c_len;
	c = (struct Column *) malloc(sizeof(struct Column) * 200);
	t = q->columns;
	
	// First get all the columns in SELECT, and create the 
	// SELECT query
	allColumnsInSelect(c, t, &ind);	
	c_len = ind;
	struct Column *allColumns = c, *column;
	UINT allColumns_size = c_len;
	
	// This creates the 
	qstr[0] = '\0';
	strcat(qstr, "SELECT \n\t");
	column = q->columns;
	while (column != NULL) {
		if (column->columnType == AGGREGATION) {
			struct Aggregation *a = column->column.aggregation;
			if (a->hasPrivateColumns == TRUE) {
				strcat(qstr, a->encQueryStr);
			} else {
				strcat(qstr, a->queryStr);
			}
		} else {
			strcat(qstr, column->columnText);
		}

		column = column->next;
		if (column != NULL) {
			strcat(qstr, ", \n\t");
		}
	}

  // Assuming nested queries do not have conjuctions
  w = q->nestedWhereClauses->w;
	if (w != NULL) {
		allColumnsInWhere(c, w, &ind);
	}
	c_len = ind;
	allColumns_size = c_len;

	nqstr[0] = 0;
	strcat(qstr, "\n FROM \n\t");
	strcat(nqstr, qstr);
	struct Table *table = q->tables;
	while (table != NULL) {
		char buf[100];
		sprintf(buf, "nested_table%d", nq->nqNum);
		strcat(nqstr, buf);
		strcat(qstr, table->name);
		if (table->alias != NULL) {
			strcat(qstr, " ");
			strcat(qstr, table->alias);
			strcat(nqstr, " ");
			strcat(nqstr, table->alias);

		}
		table = table->next;
		if (table != NULL) {
			strcat(qstr, ", \n\t");
			strcat(nqstr, ", \n\t");
		}
	}
	int cur_qstrlen = strlen(qstr);

	if (q->join != NULL) {
		strcat(qstr, " LEFT OUTER JOIN ON ");
		strcat(qstr, q->join->tableName);
		struct WhereClause *jw = q->join->w;
		char wcstr[MAXQLEN];
		wcstr[0] = 0;
		int wclen = 0;
		allPublicWhereClauses(jw, wcstr, &wclen);
		if (wclen > 0) {
			strcat(qstr, " WHERE ");
			strcat(qstr, wcstr);
		}
	}
	
	// Create the where clause string
	char wcstr[MAXQLEN];
	int wclen = 0;
	wcstr[0] = 0;
	struct WhereClause *whereClause = q->nestedWhereClauses->w;
	allPublicWhereClauses(whereClause, wcstr, &wclen);

	BOOL whereInserted = FALSE;
	// If we have at least one public where clause
	if (wclen > 0) {
		whereInserted = TRUE;
		strcat(qstr, "\n WHERE \n\t");
		strcat(qstr, wcstr);
	}
	wclen = 0;
	wcstr[0] = 0;
	whereClause = q->nestedWhereClauses->w;
	allPrivateWhereClauses(whereClause, wcstr, &wclen);
	if (wclen > 0) {
		if (!whereInserted) {
			strcat(qstr, "\n WHERE \n\t");
		} else {
			strcat(qstr, " AND ");
		}
		strcat(qstr, wcstr);
	}
	
	// Get the group by string
	if (q->groupByClauses != NULL) {
		char gbstr[MAXQLEN];
		processGroupBy(q->groupByClauses, gbstr, PRIVATE);
		if (strlen(gbstr) > 0) {
			strcat(qstr, "\n GROUP BY\n\t");
			strcat(qstr, gbstr);
		}
	}

	column = allColumns;
	ind = 0;
	nq->hasPrivate = FALSE;
	for (ind = 0; ind < allColumns_size; ind++) {
		column = allColumns + ind;
		if (column->accessType == PRIVATE) {
			nq->hasPrivate = TRUE;
		}
	}

	strcpy(nqstr + strlen(nqstr), qstr + cur_qstrlen);
	strcpy(nq->qstr, qstr);

	if (nq->hasPrivate) {
		sprintf(nq->ntName, "nested_table%d", nq->nqNum);
		strcpy(nq->qstr, nqstr);
	}
}

/*
 * This is a generic function used to replace the
 * occurrences of character a in the string by the
 * character b.
 */
void
replaceChars(char *str, char a, char b) {
	int i;
	for (i = 0; i < strlen(str); i++) {
		if (str[i] == a) {
			str[i] = b;
		}
	}
}

/*
 * This function strips the string of any extra spaces
 */
void
removeExtraSpaces(char *str) {
	int i, pl = 0, last = -1;
	for (i = 0; i < strlen(str); i++) {
		str[pl] = str[i];
		if (str[i] == ' ') {
			if (last == i-1) {
				pl--;
			}
			last = i;
		}
		pl++;
	}
	str[pl] = '\0';
}

/*
 * This is a wrapper function, which calls nestedQueryToStr()
 * for each nested query. As per the benchmark, we assume that
 * no nested query will use private attributes. Hence, this
 * function should always return false.
 */
BOOL
processNestedQueries(NestedQuery *nq) {
	BOOL hasPrivate = FALSE;
	NestedQuery *t = nq;
	while (t != NULL) {
		nestedQueryToStr(t);
		hasPrivate |= t->hasPrivate;
		t = t->next;
	}
	return hasPrivate;
}

/*
 * This function processes public queries. Basically
 * recreates the query string from the Query struct.
 */
struct query_sequence*
processPublicQuery(struct Query *q, NestedQuery *nqHead) {
	struct query_sequence *head = NULL, *tail = NULL, *qs;
	char qstr[MAXQLEN];
	UINT ind = 0, c_len;
	BOOL hasPrivateNestedQueries = processNestedQueries(nqHead);
	// This would break for Q22
	assert(!hasPrivateNestedQueries);

	struct Column *c, *t;
	c = (struct Column *) malloc(sizeof(struct Column) * 200);
	t = q->columns;
	allColumnsInSelect(c, t, &ind);	
	// Now copy all the where-clause columns
  // Assuming public queries do not have conjuctions amongst the where clauses
	struct WhereClause *w = q->nestedWhereClauses->w;
	allColumnsInWhere(c, w, &ind);
	c_len = ind;
	struct Column *allColumns = c;
	UINT allColumns_size = c_len;


	ind = 0;
	qstr[0] = '\0';
	struct Column *column;
	
	// Do the SELECT part first
	strcat(qstr, "SELECT \n\t");
	column = q->columns;
	while (column != NULL) {
		if (column->columnType == AGGREGATION) {
			strcat(qstr, column->column.aggregation->queryStr);
		} else {
			strcat(qstr, column->columnText);
		}

		column = column->next;
		if (column != NULL) {
			strcat(qstr, ", \n\t");
		}
	}
	
	// Now get the FROM part, and fill in the table names
	strcat(qstr, "\n FROM \n\t");
	struct Table *table = q->tables;
	while (table != NULL) {
		if (table->tableType == NESTED_QUERY) {
			strcat(qstr, "(");
			strcat(qstr, table->nestedQuery->qstr);
			strcat(qstr, ")");
		} else {
			strcat(qstr, table->name);
		}
		if (table->alias != NULL) {
			strcat(qstr, " ");
			strcat(qstr, table->alias);
		}
		table = table->next;
		if (table != NULL) {
			strcat(qstr, ", \n\t");
		}
	}

	char wcstr[MAXQLEN];
	int wclen = 0;
	wcstr[0] = 0;
	struct WhereClause *whereClause = q->nestedWhereClauses->w;
	allPublicWhereClauses(whereClause, wcstr, &wclen);

	// If we have at least one public where clause
	if (wclen > 0) {
		strcat(qstr, "\n WHERE \n\t");
		strcat(qstr, wcstr);
	}

	char obstr[MAXQLEN];
	if (q->orderByClauses != NULL &&
			!hasPrivateColumns(q->orderByClauses->column, allColumns)) {
		processOrderBy(q->orderByClauses, obstr);
		strcat(qstr, "\n ORDER BY\n\t");
		strcat(qstr, obstr);
	}

	strcat(qstr, "\nINTO OUTFILE '/tmp/RESULT.txt' FIELDS TERMINATED BY '|' LINES TERMINATED BY '\\n'");
	printf("%s\n\n", qstr);

	assert(head == NULL && tail == NULL);
	qs = create_qs_node(qstr, 'H', HOST_MYSQL_REQ);
	head = qs; 
	tail = qs;

	return head;
}

// Given a column name of the type <tablename.attribute>,
// this function returns the attribute name.
char *
getShortName(char *str) {
	if (str == NULL) {
		return NULL;
	}
	int i;
	for (i = 0; i < strlen(str); i++) {
		if (str[i] == '.') {
			return str + i + 1;
		}
	}
	return str;
}

/*
 * Returns the shortname of a column. If there exists an
 * alias for this column, that will be returned. Otherwise,
 * if the column is of the type, w.x_yyyyy, "w_yyyyy" would be
 * returned using getShortName()
 */
char *
getAptColumnName_Short(struct Column *c) {
	//assert(c->columnType == COLUMN);
	// ASSUMPTION, aliases are unique
	if (c->alias != NULL) {
		return c->alias;
	}

	AliasColumn *a = aHead;
	if (!inWhereClause) {
		while (a != NULL) {
			if (c->columnText && !strcmp(a->c->columnText, c->columnText)) {
				return a->c->alias;
			}
			a = a->next;
		}
	}

	return getShortName(c->columnText);
}

/**
 * This function gets the most appropriate short
 * name for the column. If there is an alias,
 * it gets that.
 *
 * If a short name is required, without the table
 * name prefix, it gets that.
 *
 * Otherwise, just returns the usual name
 */
char *
getAptColumnName(struct Column *c) {
	//assert(c->columnType == COLUMN);
	// ASSUMPTION, aliases are unique
	if (c->alias != NULL) {
		return c->alias;
	}

	AliasColumn *a = aHead;
	if (!inWhereClause || useShortName) {
		while (a != NULL) {
			if (c->columnText && !strcmp(a->c->columnText, c->columnText)) {
				return a->c->alias;
			}
			a = a->next;
		}
	}

	if (useShortName) {
		return getShortName(c->columnText);
	}
	return c->columnText;
}

// Just a debugging function
void
printAliases() {
	AliasColumn *a = aHead;
	while (a != NULL) {
		//printf("ct: %s, alias: %s\n", a->c->columnText, a->c->alias);
		a = a->next;
	}
}

// Removes new-lines and tabs from a string
void
removeNLStr(char *str) {
	if (str == NULL) {
		return;
	}
	int i;
	for (i = 0; i < strlen(str); i++) {
		if (str[i] == '\n') {
			str[i] = ' ';
		}
	}

	int p;
	char prev = -1;
	for (i = 0, p = 0; i < strlen(str); i++) {
		if ((prev == ' ' || prev == '\t') && 
				(str[i] == ' ' || str[i] == '\t')) {
			continue;
		}
		// printf("%d %d %c\n", p, i, str[i]);
		prev = str[i];
		str[p++] = str[i]; 
	}
	str[p] = '\0';
}

void
removeNewLines(struct query_sequence *q) {
	while (q != NULL ) {
		removeNLStr(q->query);
		q = q->next;
	}
}

/**
 * This is where most of the work happens. We pass it a
 * pointer to the Query struct, pointer to the head in
 * the nested query linked list, and pointer to the head
 * in the list of alias columns.
 */
struct query_sequence*
processQuery(struct Query *q, NestedQuery *nqHead, AliasColumn *aH) {
	aHead = aH;
	printAliases();
	useShortName = FALSE;
	inWhereClause = FALSE;
	struct query_sequence *head = NULL, *tail = NULL, *qs;
	char qstr[MAXQLEN];
	UINT ind = 0, c_len;
	
	// If the query does not have private attributes,
	// we can process it as a public query.
	if (!hasPrivateFields) {
		return processPublicQuery(q, nqHead);
	}

	// ASSUMPTION: The number of columns here would not exceed
	// MAXCOLS
	struct Column *c, *t;
	getPrivateColumns();
	BOOL hasPrivateNestedQueries = processNestedQueries(nqHead);	

	c = (struct Column *) malloc(sizeof(struct Column) * 200);
	t = q->columns;
	allColumnsInSelect(c, t, &ind);	
	// Now copy all the where-clause columns
	struct WhereClause *w = q->nestedWhereClauses->w;
	allColumnsInWhere(c, w, &ind);
	c_len = ind;
	struct Column *allColumns = c;
	UINT allColumns_size = c_len;


	// Create the first temp table, TEMP1
	ind = 0;
	qstr[0] = '\0';
	strcat(qstr, "CREATE TABLE TEMP1 (");
	struct Column *column;
	while (ind < c_len) {
		column = allColumns + ind;
		char *str = getAptColumnName_Short(column);
		if (str != NULL) {
			strcat(qstr, getAptColumnName_Short(column));
			strcat(qstr, " ");
			strcat(qstr, dataTypeToStr(column));
		}
		if (ind + 1 < c_len) {
			strcat(qstr, ", ");
		}
		ind++;
	}
	strcat(qstr, ");");
	printf("%s\n\n", qstr);
	assert(head == NULL && tail == NULL);
	qs = create_qs_node(qstr, 'H', HOST_SQLITE_REQ);
	head = qs; 
	tail = qs;
  
	// If there are private nested queries, we need
	// to process them separately.
	if (hasPrivateNestedQueries) {
		struct NestedQuery *nq = nqHead;
		while (nq != NULL) {
			if (!nq->hasPrivate) {
				nq = nq->next;
				continue;
			}
			struct Column *nc; 
			int nind = 0;
			nc = (struct Column *) malloc(sizeof(struct Column) * 200);
			allColumnsInSelect(nc, nq->q->columns, &nind);	
			allColumnsInWhere(nc, nq->q->nestedWhereClauses->w, &nind);
			qstr[0] = 0;
			sprintf(qstr, "CREATE TABLE %s (", nq->ntName);
			struct Column *column;
			int nc_len = nind;
			nind = 0;
			while (nind < nc_len) {
				column = nc + nind;
				char *str = getAptColumnName_Short(column);
				if (str == NULL) {
					nind++;
					continue;
				}

				strcat(qstr, str);
				strcat(qstr, " ");
				strcat(qstr, dataTypeToStr(column));
				if (nind + 1 < nc_len) {
					strcat(qstr, ", ");
				}
				nind++;
			}
			strcat(qstr, ");");
			
			strcat(qstr, "\n");
			// printf(qstr);
			qs = create_qs_node(qstr, 'H', HOST_SQLITE_REQ);
			tail->next = qs;
			tail = qs;


			// INSERT INTO TABLE
			qstr[0] = 0;
			strcat(qstr, "SELECT \n\t");

			column = nc;
			ind = 0;
			while (ind < nc_len) {
				column = allColumns + ind;
				strcat(qstr, column->columnText);
				if (ind + 1 < nc_len) {
					strcat(qstr, ", \n\t");
				}
				ind++;
			}

			strcat(qstr, "\n FROM \n\t");
			struct Table *table = nq->q->tables;
			while (table != NULL) {
				if (table->tableType == NESTED_QUERY) {
					strcat(qstr, "(");
					strcat(qstr, table->nestedQuery->qstr);
					strcat(qstr, ")");
				} else {
					strcat(qstr, table->name);
				}
				if (table->alias != NULL) {
					strcat(qstr, " ");
					strcat(qstr, table->alias);
				}
				table = table->next;
				if (table != NULL) {
					strcat(qstr, ", \n\t");
				}
			}

			strcat(qstr, "\n INTO OUTFILE '/tmp/temp_input.txt' FIELDS TERMINATED BY '|' LINES TERMINATED BY '\\n';");
			qs = create_qs_node(qstr, 'H', HOST_MYSQL_REQ);
			tail->next = qs;
			tail = qs;
			printf("%s\n", qstr);

			
			// MOVE TO TABLE NAME
			qstr[0] = 0;
			sprintf(qstr, "sqlite3 /tmp/tdb_sqlite.db '.import /tmp/temp_input.txt %s'", nq->ntName);
			printf("%s\n\n", qstr);
			qs = create_qs_node(qstr, 'H', HOST_OS_REQ);
			tail->next = qs;
			tail = qs;

			// REMOVE THE TEMP
			qstr[0] = 0;
			strcat(qstr, "rm -rf /tmp/temp_*");
			printf("%s\n\n", qstr);
			qs = create_qs_node(qstr, 'H', HOST_OS_REQ);
			tail->next = qs;
			tail = qs;
			nq = nq->next;
		}
	}

	// Create a second query for public where clauses
	qstr[0] = '\0';
	strcat(qstr, "SELECT \n\t");
	column = allColumns;
	ind = 0;
	while (ind < allColumns_size) {
		column = allColumns + ind;
		if (column->columnText != NULL) {
			strcat(qstr, column->columnText);
			if (column->alias != NULL) {
				strcat(qstr, " AS ");
				strcat(qstr, column->alias);
			}
		}
		if (ind + 1 < allColumns_size) {
			strcat(qstr, ", \n\t");
		}
		ind++;
	}

	strcat(qstr, "\n FROM \n\t");
	struct Table *table = q->tables;
	while (table != NULL) {
		strcat(qstr, table->name);
		if (table->alias != NULL) {
			strcat(qstr, " ");
			strcat(qstr, table->alias);
		}
		table = table->next;
		if (table != NULL) {
			strcat(qstr, ", \n\t");
		}
	}

  struct NestedWhereClause *n = q->nestedWhereClauses;
  BOOL firstClause = TRUE;
	char wcstr[MAXQLEN];
  int wclen;
  struct WhereClause *whereClause;

  while (n != NULL) {
	  wclen = 0;
	  wcstr[0] = 0;
	  whereClause = n->w;
	  allPublicWhereClauses(whereClause, wcstr, &wclen);

	  // If we have at least one public where clause
	  if (wclen > 0) {
      if (firstClause) {
        strcat(qstr, "\n WHERE \n\t");
        if (n->next != NULL) {
          strcat(qstr, "(");
        }
      } else {
        strcat(qstr, " OR (");
      }
      strcat(qstr, wcstr);
  	  if (!firstClause || n->next != NULL) {
        strcat(qstr, ")");
      } 
      firstClause = FALSE;
    }
    n = n->next;
  }

	char obstr[MAXQLEN];
	strcat(qstr, "\n INTO OUTFILE '/tmp/temp_input.txt' FIELDS TERMINATED BY '|' LINES TERMINATED BY '\\n';");
	printf("%s\n\n", qstr);
	qs = create_qs_node(qstr, 'H', HOST_MYSQL_REQ);
	tail->next = qs;
	tail = qs;


	// Create a temporary table out of the filtered table
	// in the SQLite db
	qstr[0] = 0;
	strcat(qstr, "sqlite3 /tmp/tdb_sqlite.db '.import /tmp/temp_input.txt TEMP1'");
	printf("%s\n\n", qstr);
	qs = create_qs_node(qstr, 'H', HOST_OS_REQ);
	tail->next = qs;
	tail = qs;


	useShortName = TRUE;
	// Create an index on TEMP1
	if (q->groupByClauses != NULL && !hasPrivateColumns(q->groupByClauses->columns, allColumns)) {
		qstr[0] = 0;
		strcat(qstr, "CREATE INDEX TEMP1_IDX ON TEMP1 (");
		char idstr[1000];
		processGroupBy(q->groupByClauses, idstr, PUBLIC);
		if (strlen(idstr) > 0) {
			strcat(qstr, idstr);
			strcat(qstr, ");");
			printf("%s\n\n", qstr);
			qs = create_qs_node(qstr, 'H', HOST_SQLITE_REQ);
			tail->next = qs;
			tail = qs;
		}
	}

	// Create a temporary output table in SQLite
	qstr[0] = 0;
	strcat(qstr, "CREATE TABLE TEMP2 (");
	column = q->columns;
	while (column != NULL) {
		if (column->columnType == COLUMN) {
			strcat(qstr, getAptColumnName(column));
			strcat(qstr, " ");
			strcat(qstr, dataTypeToStr(column));
		} else if (column->columnType == AGGREGATION) {
			processAggregation(column->column.aggregation);
			strcat(qstr, column->column.aggregation->alias);
			strcat(qstr, " ");
			// ASSUMPTION: Assuming the data-type of aggregations
			// is always an INTEGER
			strcat(qstr, dataTypeToStr(column));
		}

		column = column->next;
		if (column != NULL) {
			strcat(qstr, ", ");
		}
	}
	strcat(qstr, ")");
	printf("%s\n\n", qstr);
	qs = create_qs_node(qstr, 'H', HOST_SQLITE_REQ);
	tail->next = qs;
	tail = qs;

	// Create a temporary output table in MySQL
	printf("%s\n\n", qstr);
	qs = create_qs_node(qstr, 'H', HOST_MYSQL_REQ);
	tail->next = qs;
	tail = qs;

	qstr[0] = 0;
	strcat(qstr, "INSERT INTO TEMP2 \nSELECT\n\t");
	column = q->columns;
	while (column != NULL) {
		if (column->columnType == COLUMN) {
			strcat(qstr, getAptColumnName(column));
		} else if (column->columnType == AGGREGATION) {
			if (column->column.aggregation->hasPrivateColumns == TRUE) {
				strcat(qstr, column->column.aggregation->encQueryStr);
			} else {
				strcat(qstr, column->column.aggregation->queryStr);
			}

		}
		column = column->next;
		if (column != NULL) {
			strcat(qstr, ", \n\t");
		}
	}

	strcat(qstr, "\n FROM TEMP1");

  n = q->nestedWhereClauses;
  firstClause = TRUE;
  
  while (n != NULL) {
	  wclen = 0;
	  wcstr[0] = 0;
	  whereClause = n->w;
	  allPrivateWhereClauses(whereClause, wcstr, &wclen);

	  // If we have at least one private where clause
	  if (wclen > 0) {
      if (firstClause) {
        strcat(qstr, "\n WHERE \n\t");
        if (n->next != NULL) {
          strcat(qstr, "(");
        }
      } else {
        strcat(qstr, " OR (");
      }
      strcat(qstr, wcstr);
  	  if (!firstClause || n->next != NULL) {
        strcat(qstr, ")");
      } 
      firstClause = FALSE;
    }
    n = n->next;
  }

	char gbstr[MAXQLEN];
	processGroupBy(q->groupByClauses, gbstr, PRIVATE);
	if (strlen(gbstr) > 0) {
		strcat(qstr, "\nGROUP BY\n\t");
		strcat(qstr, gbstr);
	}

	if (q->orderByClauses != NULL) {
		struct OrderByClause *o = q->orderByClauses;
		if (hasPrivateOrderByColumns(o, allColumns)) {
			processOrderBy(o, obstr);
			strcat(qstr, "\n ORDER BY\n\t");
			strcat(qstr, obstr);
		}
	}

	printf("%s;\n\n", qstr);
	qs = create_qs_node(qstr, 'C', HOST_SQLITE_REQ);
	tail->next = qs;
	tail = qs;

	qstr[0] = 0;
	strcat(qstr, "sqlite3 -csv -separator '|' /tmp/tdb_sqlite.db 'select * from TEMP2' > /tmp/temp_output.txt");
	printf("%s\n\n", qstr);
	qs = create_qs_node(qstr, 'H', HOST_OS_REQ);
	tail->next = qs;
	tail = qs;


	qstr[0] = 0;
	strcat(qstr, "LOAD DATA LOCAL INFILE '/tmp/temp_output.txt' INTO TABLE TEMP2 FIELDS TERMINATED BY '|' LINES TERMINATED BY '\\n'");
	printf("%s\n\n", qstr);
	qs = create_qs_node(qstr, 'H', HOST_MYSQL_REQ);
	tail->next = qs;
	tail = qs;

	qstr[0] = 0;
	strcat(qstr, "rm -rf /tmp/temp_*");
	printf("%s\n\n", qstr);
	qs = create_qs_node(qstr, 'H', HOST_OS_REQ);
	tail->next = qs;
	tail = qs;

	qstr[0] = 0;
	strcat(qstr, "SELECT * FROM TEMP2 "); 
	if (q->orderByClauses != NULL) {
		struct OrderByClause *o = q->orderByClauses;
		if (!hasPrivateOrderByColumns(o, allColumns)) {
			processOrderBy(o, obstr);
			strcat(qstr, "\n ORDER BY\n\t");
			strcat(qstr, obstr);
			strcat(qstr, "\n");
		}
	}	
	strcat(qstr, " INTO OUTFILE '/tmp/RESULT.txt' FIELDS TERMINATED BY '|' LINES TERMINATED BY '\\n'");
	printf("%s\n\n", qstr);
	qs = create_qs_node(qstr, 'H', HOST_MYSQL_REQ);
	tail->next = qs;
	tail = qs;

	qstr[0] = 0;
	strcat(qstr, "DROP TABLE TEMP2");
	printf("%s\n\n", qstr);
	qs = create_qs_node(qstr, 'H', HOST_MYSQL_REQ);
	tail->next = qs;
	tail = qs;

	qstr[0] = 0;
	strcat(qstr, "DROP TABLE TEMP1");
	printf("%s\n\n", qstr);
	qs = create_qs_node(qstr, 'H', HOST_SQLITE_REQ);
	tail->next = qs;
	tail = qs;

	qstr[0] = 0;
	strcat(qstr, "DROP TABLE TEMP2");
	printf("%s\n\n", qstr); 
	qs = create_qs_node(qstr, 'H', HOST_SQLITE_REQ);
	tail->next = qs;
	tail = qs;

	if (hasPrivateNestedQueries) {
		struct NestedQuery *nq = nqHead;
		while (nq != NULL) {
			if (!nq->hasPrivate) {
				nq = nq->next;
				continue;
			}
			qstr[0] = 0;
			sprintf(qstr, "DROP TABLE %s", nq->ntName);
			strcat(qstr, "\n");
			// printf(qstr);
			qs = create_qs_node(qstr, 'H', HOST_SQLITE_REQ);
			tail->next = qs;
			tail = qs;
			nq = nq->next;
		}
	}
	// dump_query_sequence(head, 1);

	removeNewLines(head);
	return head;
}

/**
 * Populates the list of private columns, which
 * is used to check if a particular column in private.
 */
void 
getPrivateColumns() {
	char cname[200];
	struct PrivateColumn *last = NULL;
	int i;
	for (i = 0; i < attr_list_sz; i++) {
		if (attr_list[i].encrypted != 'y') {
			continue;
		}
		//fprintf(stderr, "%d : %s\n", i, attr_list[i].name);
		struct PrivateColumn *p = (struct PrivateColumn *) malloc(sizeof(struct PrivateColumn));
		strcpy(p->columnText, attr_list[i].name);
		p->next = NULL;
		if (last == NULL) {
			pColumns = p;
			last = p;
		} else {
			last->next = p;
			last = p;
		}
		pColumnsLen++; 
	}
}

/**
 * Used for debugging purposes to print the query sequences.
 */
void 
dump_query_sequence (struct query_sequence* query_seq, int requires_private) {
	struct query_sequence* temp;
	char incharge;
	int type, qno = 0;

	FILE *statfd = NULL;
	if ( (statfd = fopen("query_sequence.txt", "w")) == NULL) {
		printf("Error opening query_sequence file\n");
	}

	temp = query_seq;
	while(temp != NULL)
	{
		qno++;
		incharge = temp->incharge;
		type = temp->type;
		if(requires_private == 0 && temp->incharge == 'C')
			incharge = 'C';

		fprintf(statfd, "Q%d\n---\n%s, %s\n%s\n\n",
				qno,
				((incharge == 'C') ? "c" : "h"),
				(type == HOST_MYSQL_REQ ? "mysql" : 
				 (type == HOST_SQLITE_REQ ? "sqlite" :
					(type == HOST_OS_REQ ? "os" : "Unknown")))
				, temp->query);

		temp = temp->next;
	}

	if (statfd)
		fclose(statfd);
}


NestedQuery *
addNestedQuery(struct Query *q, NestedQuery **nqHead, NestedQuery **nqTail) {
	NestedQuery *nq = (NestedQuery *) malloc(sizeof(NestedQuery));
	nq->q = q;
	nq->next = NULL;
	if (*nqHead == NULL && *nqTail == NULL) {
		*nqHead = *nqTail = nq;
		nq->nqNum = 1;
	} else {
		(*nqTail)->next = nq;
		nq->nqNum = (*nqTail)->nqNum + 1;
		*nqTail = nq;
	}
	return nq;
}

