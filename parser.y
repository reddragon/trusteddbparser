%{
#include <stdio.h>
#include <string.h>
#include <assert.h>
#include "query.h"

extern int yylineno;
extern char *yytext;

void yyerror(const char *s);

struct Query* root;

// This is a list of nested queries
NestedQuery *nqHead, *nqTail;

// This is a list of aliases
AliasColumn *aHead, *aTail;

// This is set if any private attributes are found
BOOL hasPrivateFields;
%}

// Each node in the AST is a union of any of these
// types.
%union {
  char *id;
  struct Query *query;
  struct Column *column;
  struct Table *table;
	struct NestedWhereClause *nestedWhereClause;
  struct WhereClause *whereClause;
  struct GroupByClause *groupByClause;
  struct OrderByClause *orderByClause;
	struct Aggregation *aggregation;
	struct Expression *expression;
	struct NestedQuery *nestedQuery;
	struct Join *join;
}

// Here we assign types to each token.

%token <id> IDENTIFIER CMP_OP NUMBER STR
%token SELECT FROM WHERE AND QEND COMMA AS GROUP_BY ORDER_BY 
				DESC SUM OB CB DATE BETWEEN LIKE COUNT NOT EXISTS AVG 
				DISTINCT IN HAVING LEFT OUTER JOIN ON SUBSTR OR

%type <query> Query
%type <column> Columns Column
%type <table> Tables
%type <nestedWhereClause> NestedWhereClauses WhereConjuction
%type <whereClause> WhereClause Conditions Condition HavingClause
%type <groupByClause> GroupByClause
%type <orderByClause> OrderByClause OrderByColumns
%type <aggregation> Aggregation
%type <expression> Expression
%type <id> TableName Value StrValue NumValue AsClause ColumnOrder OptionalAlias Values OptionalValues
%type <nestedQuery> NestedQuery
%type <join> OptionalJoin

%error-verbose

%%

// What follows below are rules which define how each token is created.
// $$ is the value assigned to the token on the left of ':'. 
// $1, $2,.. and so on are the tokens to the right of ':'
Query: SELECT Columns FROM Tables OptionalJoin NestedWhereClauses GroupByClause OrderByClause OptionalEnd {
        struct Query *q = (struct Query *) malloc(sizeof(struct Query));  
        q->queryType = SELECT_QUERY;
        q->columns = $2;
        q->tables = $4;
				q->join = $5;
        q->nestedWhereClauses = $6;
        q->groupByClauses = $7;
        q->orderByClauses = $8;
        q->next = NULL;
        $$ = q;
				root = $$;
       }
;

// There can be multiple rules for defining a token.
// Here, OptionalEnd can be constructed either by the QEND token, or
// nothing at all.
OptionalEnd: QEND
					 	|	;

Tables: TableName AsClause {
          struct Table *t = (struct Table*) malloc(sizeof(struct Table));
          t->name = $1;
          t->tableType = TABLE;
          t->alias = $2;
          t->next = NULL;
          $$ = t;
        } | 
        TableName AsClause COMMA Tables {
          struct Table *t = (struct Table*) malloc(sizeof(struct Table));
          t->name = $1;
          t->tableType = TABLE;
          t->alias = $2;
          t->next = $4;
          $$ = t;
        } | '(' NestedQuery ')' OptionalValues {
          struct Table *t = (struct Table*) malloc(sizeof(struct Table));
					t->tableType = NESTED_QUERY;
					t->nestedQuery = $2;
					t->alias = $4;
					t->next = NULL;
					$$ = t;
				} 
;

OptionalJoin: LEFT OUTER JOIN IDENTIFIER ON Conditions {
							struct Join *j = (struct Join *) malloc(sizeof(struct Join));
							j->tableName = $4;
							j->w = $6;
							$$ = j;
						} | { 
							$$ = NULL;
						}
;

AsClause: TableName {
            $$ = $1;
          } | {
            $$ = NULL;
          }
;

OptionalValues: AS IDENTIFIER {
									$$ = (char *) malloc(sizeof(char) * (MAXQLEN));
									strcat($$, "AS ");
									strcat($$, $2);
								} | {
									$$ = NULL;
								}
;

GroupByClause: GROUP_BY Columns HavingClause {
                $$->columns = $2;   
								$$->conditions = $3;
               } | {
                $$ = NULL;
               }
;

HavingClause: HAVING Conditions {
								$$ = $2;
							} | {
								$$ = NULL;
							}
;

OrderByClause: ORDER_BY OrderByColumns {
						 	struct Column *p = $2->column;
							while (p != NULL) {
								p = p->next;
							}
              $$ = $2;   
             } | {
              $$ = NULL;
             }
;

OrderByColumns: Column ColumnOrder COMMA OrderByColumns {
                  struct OrderByClause *o = (struct OrderByClause *) malloc(sizeof(struct OrderByClause));
                  o->column = $1;
                  o->order = $2;
                  o->next = $4;
                  $$ = o;
                } | Column ColumnOrder {
                  struct OrderByClause *o = (struct OrderByClause *) malloc(sizeof(struct OrderByClause));
									o->column = $1;
                  o->order = $2;
                  o->next = NULL;
                  $$ = o;
                }
;

ColumnOrder: DESC {
              $$ = "DESC"; 
             } | {
              $$ = "ASC";
             }
;

Columns: Column COMMA Columns {
          $1->next = $3;
          $$ = $1;
         } |
         Column {
          $$ = $1;
         } 
;

NestedWhereClauses: WHERE '(' Conditions ')' WhereConjuction {
											struct NestedWhereClause *n = (struct NestedWhereClause *) malloc(sizeof(struct NestedWhereClause));
											n->w = $3;
											n->next = $5;
											$$ = n;
										} | WhereClause {
											struct NestedWhereClause *n = (struct NestedWhereClause *) malloc(sizeof(struct NestedWhereClause));
											n->w = $1;
											n->next = NULL;
											$$ = n;
										}  
;

WhereConjuction: OR '(' Conditions ')' WhereConjuction {
									struct NestedWhereClause *n = (struct NestedWhereClause *) malloc(sizeof(struct NestedWhereClause));
									n->w = $3;
									n->next = $5;
									$$ = n;
								} | {
									$$ = NULL;
								}
;

WhereClause: WHERE Conditions {
							$$ = $2;
             } | {
              $$ = NULL;
             }
;

Conditions: Condition AND Conditions {
              $1->next = $3;
              $$ = $1;
            } |
            Condition {
              $$ = $1;  
            }
;

Condition:  Column CMP_OP Column {
              struct WhereClause *w = (struct WhereClause*) 
                malloc(sizeof(struct WhereClause));
              // TODO Fix this
              w->lhsField = $1->columnText;
              w->rhsField = $3->columnText;
              // TODO Fix this
              w->predicateOperator = strToOperator($2);
              w->leftColumn = $1;
              w->rightColumn = $3;
              w->next = NULL;
              $$ = w;
            } |
            Column CMP_OP Value {
              struct WhereClause *w = (struct WhereClause*) 
                malloc(sizeof(struct WhereClause));
              w->lhsField = $1->columnText;
              w->rhsField = $3;
              
							w->predicateOperator = strToOperator($2);
              w->leftColumn = $1;
              w->rightColumn = NULL;
              w->next = NULL;
              $$ = w;
            } |
            Value  CMP_OP Column {
              struct WhereClause *w = (struct WhereClause*) 
                malloc(sizeof(struct WhereClause));
              w->lhsField = $1;
              w->rhsField = $3->columnText;
              
							w->predicateOperator = strToOperator($2);
              w->leftColumn = NULL;
              w->rightColumn = $3;
              w->next = NULL;
              $$ = w;
            } |
						Column BETWEEN Value AND Value {
							struct WhereClause *w = (struct WhereClause*) 
                malloc(sizeof(struct WhereClause));
              w->lhsField = $1->columnText;
              char *tv = (char *) malloc(sizeof(char) * (strlen($3) + strlen($5) + 5));
							strcpy(tv, $3);
							strcat(tv, " AND ");
							strcat(tv, $5);
							w->rhsField = tv;
              
							w->predicateOperator = BETWEEN_OP;
              w->leftColumn = $1;
              w->rightColumn = NULL;
              w->next = NULL;
              $$ = w;
						} |
						Column LIKE Value {
							struct WhereClause *w = (struct WhereClause*) 
                malloc(sizeof(struct WhereClause));
              w->lhsField = $1->columnText;
							w->rhsField = $3;
              
							w->predicateOperator = LIKE_OP;
              w->leftColumn = $1;
              w->rightColumn = NULL;
              w->next = NULL;
              $$ = w;
						} | 
						Column NOT LIKE Value {
							struct WhereClause *w = (struct WhereClause*) 
                malloc(sizeof(struct WhereClause));
              w->lhsField = $1->columnText;
							w->rhsField = $4;
              
							w->predicateOperator = NOT_LIKE_OP;
              w->leftColumn = $1;
              w->rightColumn = NULL;
              w->next = NULL;
              $$ = w;
						} | 
						EXISTS '(' NestedQuery ')' {
							struct WhereClause *w = (struct WhereClause*) 
                malloc(sizeof(struct WhereClause));
              w->predicateOperator = EX;
              w->eQuery = $3;
							w->next = NULL;
              $$ = w;
						}	|
						NOT EXISTS '(' NestedQuery ')' {
							struct WhereClause *w = (struct WhereClause*) 
                malloc(sizeof(struct WhereClause));
              w->lhsField = "";
							w->rhsField = "";
							w->predicateOperator = NEX;
              w->eQuery = $4;
							w->next = NULL;
              $$ = w;
						} |
						Column IN '(' Values ')' {
							struct WhereClause *w = (struct WhereClause*) 
                malloc(sizeof(struct WhereClause));
							w->predicateOperator = IN_OP;
							w->lhsField = $1->columnText;
							w->rhsField = $4;
              w->leftColumn = $1;
              w->rightColumn = NULL;
							w->next = NULL;
              $$ = w;
						} |
						Column IN '(' NestedQuery ')' {
							struct WhereClause *w = (struct WhereClause*) 
                malloc(sizeof(struct WhereClause));
              w->predicateOperator = IN_OP;
							w->lhsField = $1->columnText;
							w->rhsField = NULL;
							w->leftColumn = $1;
              w->rightColumn = NULL;
              w->eQuery = $4;
							w->next = NULL;
              $$ = w;
						} | 
						Column CMP_OP '(' NestedQuery ')' {
							struct WhereClause *w = (struct WhereClause*) 
                malloc(sizeof(struct WhereClause));
              w->predicateOperator = strToOperator($2);
							w->lhsField = $1->columnText;
							w->rhsField = NULL;
							w->leftColumn = $1;
              w->rightColumn = NULL;
              w->eQuery = $4;
							w->next = NULL;
              $$ = w;
						}
;

NestedQuery: Query {
					 		$$ = addNestedQuery($1, &nqHead, &nqTail);
						}
;

Values: Values COMMA Value {
					$$ = $1;
					strcat($$, ", ");
					strcat($$, $3);
				} |
				Value {
					char *str = (char *) malloc(sizeof(char) * (MAXQLEN + 10));
					strcpy(str, $1);
					$$ = str;
				}

Value:  NumValue {
          $$ = $1;
        } |
        StrValue {
          $$ = $1;
        } 
;

StrValue: STR {
            $$ = $1;
          }
;

NumValue: NUMBER {
            $$ = $1;
          }
;

Column: IDENTIFIER OptionalAlias {
          struct Column *c = (struct Column*) malloc(sizeof(struct Column));
          c->column.columnText = $1;
          c->columnType = COLUMN;
          c->accessType = getAccessType($1);
					if (c->accessType == PRIVATE) {
						hasPrivateFields = TRUE;
					}
          c->dataType = getDataType($1);
          c->columnText = $1;
					c->alias = $2;
					c->next = NULL;
					
					if (c->alias != NULL) {
						AliasColumn *a = (AliasColumn *) malloc(sizeof(AliasColumn));
						a->c = c;
						a->next = NULL;
						if (aHead == NULL) {
							aHead = aTail = a;
						} else {
							aTail->next = a;
							aTail = a;
						}
					}
          $$ = c;
        } | Aggregation {
					struct Column *c = (struct Column*) malloc(sizeof(struct Column));
          c->column.aggregation = $1;
          c->columnType = AGGREGATION;
					
					c->alias = $1->alias;
					c->next = NULL;
          $$ = c;
				} | '*' OptionalAlias {
					struct Column *c = (struct Column*) malloc(sizeof(struct Column));
          c->columnText = (char *) malloc(sizeof(char)*3);
					strcpy(c->columnText, "*");
					c->columnType = ALLCOLUMNS;
					c->alias = $2;
					c->next = NULL;
          $$ = c;
				}
;

Aggregation: SUM '(' Expression ')' OptionalAlias {
					 		$$ = (struct Aggregation *) malloc (sizeof(struct Aggregation));
							memset($$, 0, sizeof(struct Aggregation));
              $$->aggregationType = AGGREGATION_SUM;
							$$->expression = $3;
							$$->alias = $5;
							$$->hasPrivateColumns = FALSE;
					 	 } | COUNT '(' '*' ')' OptionalAlias {
						 	$$ = (struct Aggregation *) malloc (sizeof(struct Aggregation));
							memset($$, 0, sizeof(struct Aggregation));
              $$->aggregationType = AGGREGATION_COUNT;
							$$->expression = NULL;
							$$->alias = $5;
							$$->hasPrivateColumns = FALSE;
						 } | COUNT '(' DISTINCT Expression ')' OptionalAlias {
						 	$$ = (struct Aggregation *) malloc (sizeof(struct Aggregation));
							memset($$, 0, sizeof(struct Aggregation));
              $$->aggregationType = AGGREGATION_COUNT_D;
							$$->expression = $4;
							$$->alias = $6;
							$$->hasPrivateColumns = FALSE;
						 } | AVG '(' Expression ')' OptionalAlias {
					 		$$ = (struct Aggregation *) malloc (sizeof(struct Aggregation));
							$$->aggregationType = AGGREGATION_AVG;
							$$->expression = $3;
							$$->alias = $5;
							$$->hasPrivateColumns = FALSE;
					 	 } | COUNT '(' Expression ')' OptionalAlias {
						 	$$ = (struct Aggregation *) malloc (sizeof(struct Aggregation));
							memset($$, 0, sizeof(struct Aggregation));
              $$->aggregationType = AGGREGATION_COUNT;
							$$->expression = $3;
							$$->alias = $5;
							$$->hasPrivateColumns = FALSE;
						 } | SUBSTR '(' Expression COMMA NumValue COMMA NumValue ')' OptionalAlias {
						 		$$ = (struct Aggregation *) malloc (sizeof(struct Aggregation));
							memset($$, 0, sizeof(struct Aggregation));
              $$->aggregationType = AGGREGATION_SUBSTR;
							$$->expression = $3;
							$$->alias = $9;
							$$->hasPrivateColumns = FALSE;
						 }
;

OptionalAlias:	AS IDENTIFIER {
									$$ = $2;										
								} | {
									$$ = NULL;
								}
;

Expression: Expression '*' Expression {
							$$ = (struct Expression *) malloc (sizeof(struct Expression));
							$$->expressionType = NESTED_EXPRESSION;
							struct NestedExpression *ne = (struct NestedExpression *) malloc(sizeof(struct NestedExpression));
							ne->lhs = $1;
							ne->rhs = $3;
							ne->binaryOp = '*';
							$$->exp.ne = ne;
						} | Expression '+' Expression {
							$$ = (struct Expression *) malloc (sizeof(struct Expression));
							$$->expressionType = NESTED_EXPRESSION;
							struct NestedExpression *ne = (struct NestedExpression *) malloc(sizeof(struct NestedExpression));
							ne->lhs = $1;
							ne->rhs = $3;
							ne->binaryOp = '+';
							$$->exp.ne = ne;
						} | Expression '-' Expression {
							$$ = (struct Expression *) malloc (sizeof(struct Expression));
							$$->expressionType = NESTED_EXPRESSION;
							struct NestedExpression *ne = (struct NestedExpression *) malloc(sizeof(struct NestedExpression));
							ne->lhs = $1;
							ne->rhs = $3;
							ne->binaryOp = '-';
							$$->exp.ne = ne;
						} | '(' Expression ')' {
							$$ = $2;
						} | Column {
							$$ = (struct Expression *) malloc (sizeof(struct Expression));
							$$->expressionType = COLUMN_EXPRESSION;
							$$->exp.column = $1; 
						} | Value {
							$$ = (struct Expression *) malloc (sizeof(struct Expression));
							$$->expressionType = VALUE_EXPRESSION;
							$$->exp.value = $1; 
						}
;

TableName: IDENTIFIER {
            $$ = $1;
           }
;

%%

void 
yyerror(const char *s) {
  fprintf(stderr, "Error on line %d: %s\n", yylineno, s);
	exit(1);
}

struct query_sequence *
parse_str(char *str, struct Query **q, BOOL *hasPrivate) {
	yy_scan_string(str);
	root = *q;
	nqHead = nqTail = NULL;
	aHead = aTail = NULL;
	hasPrivateFields = FALSE;
	int ret = yyparse();
	if (ret != 0) {
		return NULL;
	}
	*q = root;
	*hasPrivate = hasPrivateFields;
	return processQuery(root, nqHead, aHead);
}
