#include <stdio.h>
#include <assert.h>
#include "parser.tab.h"
#include "query.h"
#include "test_queries.h"

extern struct query_sequence* parse_str(char *str, struct Query **q, BOOL *hasPrivate);
extern struct Query *root;
extern NestedQuery *nqHead, *nqTail;
extern AliasColumn *aHead, *aTail;
extern BOOL hasPrivateFields;

extern int parse();

int main(int argc, char **argv) {
	getPrivateColumns();
	
  struct Query *q;
  BOOL hasPrivateFields;
	
	if (argc == 2) {
		freopen(argv[1], "r", stdin);
		aHead = aTail = NULL;
		nqHead = nqTail = NULL;
		yyparse();
		q = root;
		hasPrivateFields = FALSE;
		struct query_sequence *ret = processQuery(q, nqHead, aHead);
		dump_query_sequence(ret, 1);
	} 
	return 0;
}
