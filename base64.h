/*
** Translation Table as described in RFC1113
*/
static const char cb64[]="ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/";

/*
** Translation Table to decode (created by author)
*/
static const char cd64[]="|$$$}rstuvwxyz{$$$$$$$>?@ABCDEFGHIJKLMNOPQRSTUVW$$$$$$XYZ[\\]^_`abcdefghijklmnopq";

void encode(char *input, char *output, int datalen);
void encodeblock( unsigned char in[3], unsigned char out[4], int len );

void decodeblock( unsigned char in[4], unsigned char out[3] );
int decode(char *input, char *output);
