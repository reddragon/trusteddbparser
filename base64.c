#include <string.h>
#include "base64.h"

/*
** encodeblock
**
** encode 3 8-bit binary bytes as 4 '6-bit' characters
*/
void encodeblock( unsigned char in[3], unsigned char out[4], int len )
{
    out[0] = cb64[ in[0] >> 2 ];
    out[1] = cb64[ ((in[0] & 0x03) << 4) | ((in[1] & 0xf0) >> 4) ];
    out[2] = (unsigned char) (len > 1 ? cb64[ ((in[1] & 0x0f) << 2) | ((in[2] & 0xc0) >> 6) ] : '=');
    out[3] = (unsigned char) (len > 2 ? cb64[ in[2] & 0x3f ] : '=');
}

/*
** encode
**
** base64 encode a stream adding padding and line breaks as per spec.
*/
void encode(char *input, char *output, int datalen)
{
    unsigned char in[3], out[4];
    int i, len, blocksout = 0;
    int count = 0, ind = 0;


    while(count < datalen) {
        len = 0;
        for( i = 0; i < 3; i++ ) {
            in[i] = input[count++];
            if( count <= datalen ) {
                len++;
            }
            else {
                in[i] = 0;
            }
        }
        if( len ) {
            encodeblock( in, out, len );
            for( i = 0; i < 4; i++ ) {
		output[ind++] = out[i];
            }
            blocksout++;
        }
        if( blocksout >= (datalen/4) )  {
            blocksout = 0;
        }
    }
}

/*
** decodeblock
**
** decode 4 '6-bit' characters into 3 8-bit binary bytes
*/
void decodeblock( unsigned char in[4], unsigned char out[3] )
{
    out[ 0 ] = (unsigned char ) (in[0] << 2 | in[1] >> 4);
    out[ 1 ] = (unsigned char ) (in[1] << 4 | in[2] >> 2);
    out[ 2 ] = (unsigned char ) (((in[2] << 6) & 0xc0) | in[3]);
}

/*
** decode
**
** decode a base64 encoded stream discarding padding, line breaks and noise
*/
int decode(char *input, char *output)
{
    unsigned char in[4], out[3], v;
    int i, len;
    int ind = 0, outind = 0;
    int datalen = strlen(input);

    while( ind < datalen ) {
        for( len = 0, i = 0; i < 4 && ind < datalen; i++ ) {
            v = 0;
            while( ind < datalen && v == 0 ) {
                v = (unsigned char) input[ind++];
                v = (unsigned char) ((v < 43 || v > 122) ? 0 : cd64[ v - 43 ]);
                if( v ) {
                    v = (unsigned char) ((v == '$') ? 0 : v - 61);
                }
            }
            if( ind < datalen ) {
                len++;
                if( v ) {
                    in[ i ] = (unsigned char) (v - 1);
                }
            }
            else {
                in[i] = 0;
            }
        }
        if( len ) {
            decodeblock( in, out );
            for( i = 0; i < len - 1; i++ ) {
		output[outind++] = out[i];
            }
        }
    }

    return outind;
}

unsigned char rand_padbit ()
{
	return rand() % 256;
}
