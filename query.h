#ifndef _PARSER_H_
#define _PARSER_H_

// Required for the query_sequence structure definition
#include "schema.h"
// For defining the macros used for type in query_sequence structs
// TODO Uncomment this when integrating with the main parser
// #include "rteX.h"
// TODO Comment the following three #defines when integrating with
// the main parser
#define HOST_MYSQL_REQ 0x0001
#define HOST_OS_REQ 0x0002
#define HOST_SQLITE_REQ 0x0004

#define BOOL unsigned char
#define TRUE 1
#define FALSE 0
#define UINT unsigned int
#define MAXQLEN 10000
#define MAXCOLS 200

struct PrivateColumn *pColumns;
UINT pColumnsLen;

enum QueryType {
  SELECT_QUERY
};

struct Query {
  enum QueryType queryType;
  struct Column *columns;
  struct NestedWhereClause *nestedWhereClauses;
  // struct WhereClause *whereClauses;
  struct OrderByClause *orderByClauses;
  struct GroupByClause *groupByClauses;
  struct Table *tables;
  struct Query *next;
  struct Join *join;
};

typedef struct NestedQuery {
  struct Query *q;
  struct NestedQuery *next;
  UINT nqNum;
  BOOL hasPrivate;
  char qstr[MAXQLEN];
  char nqstr[MAXQLEN];
  char ntName[200]; // Name of the nested table
} NestedQuery;

enum ColumnType {
  COLUMN,
  AGGREGATION,
  ALLCOLUMNS
};

enum AccessType {
  PUBLIC,
  PRIVATE
};

enum DataType {
  VARCHAR,
  INTEGER
};

struct Column {
  union {
    char *columnText;
    struct Aggregation *aggregation;
  } column;
  enum ColumnType columnType;
  enum AccessType accessType;
  enum DataType dataType;
  char *columnText;
  struct Column *next;
  char *alias;
};

typedef struct AliasColumn {
  struct Column *c;
  struct AliasColumn *next;
} AliasColumn;

enum PredicateOperator {
  GEQ,
  LEQ,
  EQ,
  GT,
  LT,
  NEQ,
  BETWEEN_OP,
  LIKE_OP,
  NOT_LIKE_OP,
  EX,
  NEX,
  IN_OP
};

struct WhereClause {
  enum PredicateOperator predicateOperator;
  char *lhsField, *rhsField;
  struct Column *leftColumn, *rightColumn;
  struct NestedQuery *eQuery;
  struct WhereClause *next;
};

struct NestedWhereClause {
  struct WhereClause *w;
  struct NestedWhereClause *next;
};

struct GroupByClause {
  struct Column *columns;
  struct WhereClause *conditions;
};

struct OrderByClause {
  char *field;
  char *order;
  struct Column *column;
  struct OrderByClause *next;
};

enum TableType {
  TABLE,
  NESTED_QUERY
};

struct Table {
  enum TableType tableType;
  char *name;
  char *alias;
  struct Table *next;
  struct NestedQuery *nestedQuery;
};

struct Join {
  char *tableName;
  struct WhereClause *w;
};

enum ExpressionType {
  NESTED_EXPRESSION,
  VALUE_EXPRESSION,
  COLUMN_EXPRESSION
};

struct Expression {
  union {
    struct NestedExpression *ne;
    char *value;
    struct Column *column;
  } exp;
  enum ExpressionType expressionType;
};

struct NestedExpression {
  struct Expression *lhs, *rhs;
  char binaryOp;
};

enum AggregationType {
  AGGREGATION_SUM,
  AGGREGATION_COUNT,
  AGGREGATION_COUNT_D,
  AGGREGATION_AVG,
  AGGREGATION_SUBSTR
};

struct Aggregation {
  enum AggregationType aggregationType;
  struct Expression *expression;
  UINT *allColumnsLen;
  struct Column *allColumns;
  char *queryStr;
  char *encQueryStr;
  char *encryptedQueryStr;
  char *alias;
  BOOL hasPrivateColumns;
};

struct PrivateColumn {
  char columnText[200];
  struct PrivateColumn *next;
};

void printQuery(struct Query *q);
enum PredicateOperator strToOperator(char* str);
enum ColumnType getColumnType(char *str);
enum AccessType getAccessType(char *str);
enum DataType getDataType(char *str);
char *dataTypeToStr(struct Column *column);
struct query_sequence *processQuery(struct Query *q, NestedQuery *nqHead, AliasColumn *aHead);
char *whereClauseToStr(struct WhereClause *w, BOOL allowPrivates);
BOOL isPrivateColum(struct Column *c);
void getPrivateColumns();
void dump_query_sequence (struct query_sequence* query_seq, int requires_private);
NestedQuery *addNestedQuery(struct Query *q, NestedQuery **nqHead, NestedQuery **nqTail);
void nestedQueryToStr(NestedQuery *nq);
char *getShortName(char *str);
char *getAptColumnName(struct Column *c);
#endif

