#ifndef _TEST_QUERIES_H
#define _TEST_QUERIES_H

//1,3,4,5,6,7,8,9,10,12,13,14,16,17,18,19,21,22

char *queries[23] = {
//0
"",
//1
"select l.l_returnflag, l.l_linestatus, sum(l.l_quantity) as sum_qty, sum(l.l_extendedprice) as sum_base_price, sum(l.l_extendedprice*(1-l.l_discount)) as sum_disc_price, sum(l.l_extendedprice*(1-l.l_discount)*(1+l.l_tax)) as sum_charge, avg(l.l_quantity) as avg_qty, avg(l.l_extendedprice) as avg_price, avg(l.l_discount) as avg_disc, count(*) as count_order from lineitem l where l.l_shipdate <= '1992-03-02' group by l.l_returnflag, l.l_linestatus order by l.l_returnflag, l.l_linestatus",
//2
 "",
//3
"select l.l_orderkey, sum(l.l_extendedprice*(1-l.l_discount)) as revenue, o.o_orderdate, o.o_shippriority from customer c, orders o, lineitem l where c.c_mktsegment = 'BUILDING' and c.c_custkey = o.o_custkey and l.l_orderkey = o.o_orderkey and o.o_orderdate < '1995-03-15' and l.l_shipdate > '1995-03-15' group by l.l_orderkey, o.o_orderdate, o.o_shippriority order by revenue desc, o.o_orderdate",
//4
"select o.o_orderpriority, count(*) as order_count from orders o where o.o_orderdate >= '1993-07-01' and o.o_orderdate < '1993-10-01' and exists (select * from lineitem l where l.l_orderkey = o.o_orderkey and l.l_commitdate < l.l_receiptdate) group by o_orderpriority order by o_orderpriority",
//5
"select n.n_name, sum(l.l_extendedprice * (1 - l.l_discount)) as revenue from customer c, orders o, lineitem l, supplier s, nation n, region r where 	c.c_custkey = o.o_custkey and l.l_orderkey = o.o_orderkey and l.l_suppkey = s.s_suppkey and c.c_nationkey = s.s_nationkey and s.s_nationkey = n.n_nationkey and n.n_regionkey = r.r_regionkey and r.r_name = 'ASIA' and o.o_orderdate >= '1994-01-01' and o.o_orderdate < '1995-01-01' group by n.n_name order by revenue desc",
//6
"select sum(l.l_extendedprice*l.l_discount) as revenue from lineitem l where l.l_shipdate >= '1994-01-01' and l.l_shipdate < '1994-03-01' and l.l_discount  between 0.01 and 0.02 and l.l_quantity < 24",
//7
"select n1.n_name as supp_nation, n2.n_name as cust_nation, l.l_shipdate as l_year, sum(l.l_extendedprice * (1 - l.l_discount)) as volume from supplier s, lineitem l, orders o, customer c, nation n1, nation n2 where s.s_suppkey = l.l_suppkey and o.o_orderkey = l.l_orderkey and c.c_custkey = o.o_custkey and s.s_nationkey = n1.n_nationkey and c.c_nationkey = n2.n_nationkey and n1.n_name in ('FRANCE', 'GERMANY') and n2.n_name in ('FRANCE', 'GERMANY')	and n1.n_name <> n2.n_name and l.l_shipdate  between '1995-01-01' and '1996-02-01' group by supp_nation, cust_nation, l_year order by supp_nation, cust_nation, l_year",
//8
"select o.o_orderdate as o_year, sum(l.l_extendedprice * (1-l.l_discount)) as volume from part p, supplier s, lineitem l, orders o, customer c, nation n, region r where p.p_partkey = l.l_partkey and s.s_suppkey = l.l_suppkey and l.l_orderkey = o.o_orderkey and o.o_custkey = c.c_custkey and c.c_nationkey = n.n_nationkey and n.n_regionkey = r.r_regionkey and r.r_name = 'ASIA' and o.o_orderdate between '1995-01-01' and '1995-02-31' and p.p_type like '%BRASS%' group by o_year order by o_year",
//9
"select n.n_name as nation, o.o_orderdate as o_year, sum(l.l_extendedprice * (1 - l.l_discount) - ps.ps_supplycost * l.l_quantity) as amount from part p, supplier s, lineitem l, partsupp ps, orders o, nation n where s.s_suppkey = l.l_suppkey and ps.ps_suppkey = l.l_suppkey and ps.ps_partkey = l.l_partkey and p.p_partkey = l.l_partkey and o.o_orderkey = l.l_orderkey and s.s_nationkey = n.n_nationkey and p.p_name like '%green%' and l_shipdate between '1995-01-01' and '1995-02-01' group by nation, o_year order by nation, o_year desc",
//10
"select c.c_custkey, c.c_name, sum(l.l_extendedprice * (1 - l.l_discount)) as revenue, c.c_acctbal, n.n_name, c.c_address, c.c_phone, c.c_comment from customer c, orders o, lineitem l, nation n where c.c_custkey = o.o_custkey and l.l_orderkey = o.o_orderkey and o.o_orderdate >= '1993-10-01' and o.o_orderdate < '1993-10-20' and l.l_returnflag = 'R' and c.c_nationkey = n.n_nationkey group by c.c_custkey, c.c_name, c.c_acctbal, c.c_phone, c.c_name, c.c_address, c.c_comment order by revenue desc",
//11
"",
//12
"select l_shipmode, sum(l_quantity) as high_line_count, sum(o_totalprice) as low_line_count from orders, lineitem where o_orderkey = l_orderkey and  l_shipmode in ('MAIL', 'SHIP') and l_commitdate < l_receiptdate and l_shipdate < l_commitdate and l_receiptdate >= '1994-01-01' and l_receiptdate < '1995-01-01' group by  l_shipmode order by l_shipmode",
//13
"select c_count, count(*) as custdist from (select c_custkey, count(o_orderkey) from customer, orders where c_custkey = o_custkey and o_comment not like '%special%deposits%' group by c_custkey) as c_orders group by c_count order by custdist desc, c_count desc",
//14
"select sum(l.l_extendedprice * (1 - l.l_discount)) as promo_revenue from lineitem l, part p where l.l_partkey = p.p_partkey and l.l_shipdate >= '1993-01-01' and l.l_shipdate < '1993-02-01'",
//15
"",
//16
"select p.p_brand, p.p_type, p.p_size, count(distinct ps.ps_suppkey) as supplier_cnt from partsupp ps, part p, supplier s where p.p_partkey = ps.ps_partkey and p.p_brand <> 'Brand#34' and p.p_type not like '%TIN%' and p.p_size in ('1', '2', '3', '4', '5', '6', '7', '8') and ps.ps_suppkey = s.s_suppkey and s.s_comment not like '%Customer%Complaints%' and p.p_comment like '%slyly%' group by p_brand, p_type, p_size order by supplier_cnt desc, p_brand, p_type, p_size",
//17
"select sum(l.l_extendedprice) as avg_yearly from lineitem l, part p where p.p_partkey = l.l_partkey and p.p_brand = 'Brand#23' and p.p_container = 'MED BOX' and l.l_quantity < (select avg(l1.l_quantity) from lineitem l1 where l1.l_partkey = p.p_partkey) and l.l_shipdate >= '1993-01-01' and l.l_shipdate < '1993-02-01'",
//18
"select c.c_name, c.c_custkey, o.o_orderkey, o.o_orderdate, o.o_totalprice, sum(l.l_quantity) as squantity from customer c, orders o, lineitem l where o.o_orderkey in (select l1.l_orderkey from lineitem l1 where l1.l_shipdate >= '1993-01-01' and l1.l_shipdate < '1993-01-30' group by l1.l_orderkey having sum(l1.l_quantity) > 3) and c.c_custkey = o.o_custkey and o.o_orderkey = l.l_orderkey and o.o_orderdate >= '1993-01-01' and o.o_orderdate < '1993-01-02' group by c.c_name, c.c_custkey, o.o_orderkey, o.o_orderdate, o.o_totalprice order by o.o_totalprice desc, o.o_orderdate",
//19
"select sum(l.l_extendedprice * (1 - l.l_discount) ) as revenue from lineitem l, part p where (p.p_partkey = l.l_partkey and p.p_brand = 'Brand#12'  and p.p_container in ( 'SM CASE', 'SM BOX', 'SM PACK', 'SM PKG') and l.l_quantity >= 1 and l.l_quantity <= 11 and p.p_size between 1 and 5 and l.l_shipmode in ('AIR', 'AIR REG') and l.l_shipinstruct = 'DELIVER IN PERSON') or (p.p_partkey = l.l_partkey and p.p_brand = 'Brand#23' and p.p_container in ('MED BAG', 'MED BOX', 'MED PKG', 'MED PACK') and l.l_quantity >= 10 and l.l_quantity <= 20 and p.p_size between 1 and 10 and l.l_shipmode in ('AIR', 'AIR REG') and l.l_shipinstruct = 'DELIVER IN PERSON') or (p.p_partkey = l.l_partkey and p.p_brand = 'Brand#34' and p.p_container in ('LG CASE', 'LG BOX', 'LG PACK', 'LG PKG') and l.l_quantity >= 20 and l.l_quantity <= 30 and p.p_size between 1 and 15 and l.l_shipmode in ('AIR', 'AIR REG') and l.l_shipinstruct = 'DELIVER IN PERSON')",
//20
"" ,
//21
"select s_name, count(*) as numwait from supplier s, lineitem l1, orders o, nation n where s.s_suppkey = l1.l_suppkey and o.o_orderkey = l1.l_orderkey  and o.o_orderstatus = 'F'and l1.l_receiptdate > l1.l_commitdate and exists (select * from lineitem l2 where l2.l_orderkey = l1.l_orderkey and l2.l_suppkey <> l1.l_suppkey) and not exists (select * from lineitem l3 where l3.l_orderkey = l1.l_orderkey and l3.l_suppkey <> l1.l_suppkey and l3.l_receiptdate > l3.l_commitdate) and s.s_nationkey = n.n_nationkey and n.n_name = 'SAUDI ARABIA' group by s.s_name order by numwait desc, s.s_name",
//22
"select c_phone as cntrycode, count(*) as numcust, sum(c_acctbal) as totacctbal from customer where c_phone in ('13','31','23','29','30','18','17')  and c_acctbal > (select avg(c_acctbal) from customer where c_acctbal > 0.00 and c_phone in ('13','31','23','29','30','18','17')) and not exists (select * from orders where o_custkey = c_custkey)"
}; //end array
#endif
